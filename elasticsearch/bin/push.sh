#!/bin/sh
# Run from within ./tendermint directory

USERNAME=seductive
IMAGE=ixelasticsearch

docker build -t ${USERNAME}/${IMAGE}:latest .
docker push ${USERNAME}/${IMAGE}:latest
