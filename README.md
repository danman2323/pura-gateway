PURA Masternode Gateway
=======================
Goal: allow users who have 100K PURA create a masternode directly from their wallets

Building
-----------------------
All backend modules are wrapped in docker containers.
We also have docker-compose to run development environment. To build develpment environment run:

``` bash
docker-compose run --build
```

To stop development environment run:

``` bash
docker-compose down
```

To run tests you need to setup VPS with SSH and put it's ssh credentials in docker-compose-common.yml file.  Pls update next params:
```
# Port where remote docker is running
GENERIC_ENGINE_PORT 

# Address of remote vps
GENERIC_IP_ADDRESS  

# Path to ssh key
GENERIC_SSH_KEY: '/etc/ssh/ssh_config/id_rsa'  

# SSH user
GENERIC_SSH_USER: 'root' 

# SSH port
GENERIC_SSH_PORT: 22 
```
[See example](https://docs.docker.com/machine/drivers/generic/#example)

Then run:
``` bash
docker-compose run app python manage.py test pool 
```

App containers
-----------------------
Application consists of next containers:

| Directory        | Compose Name          | Purpose                                               |
|------------------|:----------------------|:------------------------------------------------------|
| app              | app                   | All backend and tasks source code is here             |
| app              | app_setup             | Module that configures backend and seeds DB           |
| -                | postgres              | PostgreSQL Database                                   |
| -                | redis                 | Redis database to store queues                        |
| -                | worker                | Celery workers are executed in this container         |
| -                | worker_beat           | Scheduler that creates tasks and stores them in redis |

API
-----------------------
You can find docs by following the link:
http://localhost:8000/docs


Process
-----------------------
* To create masternode send POST request to http://localhost:8000/pool/masternode (described above)
* To check the masternode status send GET request to  http://localhost:8000/pool/masternode/<masternode_priv_key>
* To delete the masternode send DELETE request to http://localhost:8000/pool/masternode/<masternode_priv_key>

Masternodes have next statuses (machine_status):
* NONE            = "NON" # masternode was not deployed yet
* CREATING        = "CRE" # deployment is in process 
* PROVISIONING    = "PRO" # provisioning is in process
* RUNNING         = "RUN" # mn is running 
* STOPPED         = "STO" # mn is destroying
* FAILED          = "FAI" # mn was not deployed due to error
* DELETED         = "DEL" # mn is deleted

Admin Panel
-----------------------
To create and stop masternodes via user interface pls find the admin panel:
http://localhost:8000/admin

* Login: admin
* Password: 123456789
