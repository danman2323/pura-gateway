from rest_framework import serializers
from rest_framework.validators import UniqueValidator

from .models import Masternode
import json

class MasternodeSerializer(serializers.ModelSerializer):

    masternode_priv_key = serializers.CharField(required=True, allow_blank=False, allow_null=False, validators=[UniqueValidator(queryset=Masternode.objects.all())])
    collateral_output_txid = serializers.CharField(required=True, allow_blank=False, allow_null=False)
    collateral_output_index = serializers.IntegerField(required=True)
    testnet = serializers.BooleanField(default=True)

    machine = serializers.SerializerMethodField('get_machine_status_json')
    def get_machine_status_json(self, obj):
        if obj.machine_info:
            return json.loads(obj.machine_info)
        return {}

    class Meta(object):
        model = Masternode
        read_only_fields = ['machine_status', 'machine_created_at']
        exclude = ['machine_info', 'machine_driver', 'machine_logs']

    def create(self, validated_data):
        masternode = Masternode.objects.create_masternode(
            masternode_priv_key=validated_data['masternode_priv_key'],
            collateral_output_txid=validated_data['collateral_output_txid'],
            collateral_output_index=validated_data['collateral_output_index'],
            testnet=validated_data['testnet'],
        )
        return masternode