from django.test import TestCase
from rest_framework.test import APITestCase
from rest_framework import status
from rest_framework.reverse import reverse
from django.conf import settings
from mngateway import settings

from .models import Masternode
from .provisioning.creator import MasternodeCreator, MasternodeCreateError
from .provisioning.rpc_checker import RPCChecker

class PoolMasternodeCreateTestCase(APITestCase):

    masternode_priv_key = "93HaYBVUCYjEMeeH1Y4sBGLALQZE1Yc1K64xiqgX37tGBDQL8Xg"
    collateral_output_txid = "2bcd3c84c84f87eaa86e4e56834c92927a07f9e18718810b92e0d0324456a67c"
    collateral_output_index = 0
    testnet = True

    def setUp(self):
        settings.DEBUG = True

    def test_create_new_masternode(self):
        new_masternode = Masternode.objects.create_masternode(masternode_priv_key=self.masternode_priv_key,
                                                          collateral_output_txid=self.collateral_output_txid,
                                                          collateral_output_index=self.collateral_output_index,
                                                          testnet=self.testnet,
                                                          machine_driver=Masternode.GENERIC)
        new_masternode.save()

        self.assertEqual(Masternode.objects.count(), 1)

        masternode = Masternode.objects.first()
        self.assertEqual(masternode.id, new_masternode.id)

        self.assertEqual(masternode.machine_status, Masternode.NONE)


class PoolMasternodeCreateAPITestCase(APITestCase):
    masternode_priv_key = "93HaYBVUCYjEMeeH1Y4sBGLALQZE1Yc1K64xiqgX37tGBDQL8Xg"
    collateral_output_txid = "2bcd3c84c84f87eaa86e4e56834c92927a07f9e18718810b92e0d0324456a67c"
    collateral_output_index = 0

    def setUp(self):
        settings.DEBUG = True

    def test_create_new_masternode(self):
        data = {
            'masternode_priv_key': self.masternode_priv_key,
            'collateral_output_txid': self.collateral_output_txid,
            'collateral_output_index': self.collateral_output_index,
            'testnet': True
        }
        url = reverse("pool:masternode_create")
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Masternode.objects.count(), 1)

    def test_create_masternode_with_existing_priv_key(self):
        data = {
            'masternode_priv_key': self.masternode_priv_key,
            'collateral_output_txid': self.collateral_output_txid,
            'collateral_output_index': self.collateral_output_index,
            'testnet': True
        }
        url = reverse("pool:masternode_create")
        response = self.client.post(url, data, format='json')

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Masternode.objects.count(), 1)

        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(Masternode.objects.count(), 1)

# class PoolCreateServerTaskTestCase(APITestCase):
#
#     masternode_priv_key = "93HaYBVUCYjEMeeH1Y4sBGLALQZE1Yc1K64xiqgX37tGBDQL8Xg"
#     collateral_output_txid = "2bcd3c84c84f87eaa86e4e56834c92927a07f9e18718810b92e0d0324456a67c"
#     collateral_output_index = 0
#     testnet = True
#
#     def tearDown(self):
#         list_masternoded = Masternode.objects.all()
#         for masternode in list_masternoded:
#             creator = MasternodeCreator(masternode)
#             print("Removing container from: " + masternode.get_machine_name())
#             creator.remove_server()
#             self.assertEqual(masternode.machine_status, Masternode.NONE)
#
#         list_machines = creator.machine.ls()
#         # FIXME: there is always one active machine with empty name
#         self.assertEqual(len(list_machines), 1, "Machine should be deleted from: $ docker-machine ls")
#
#     def setUp(self):
#         settings.DEBUG = True
#         self.masternode = Masternode.objects.create_masternode(masternode_priv_key=self.masternode_priv_key,
#                                                               collateral_output_txid=self.collateral_output_txid,
#                                                               collateral_output_index=self.collateral_output_index,
#                                                               testnet=self.testnet,
#                                                               machine_driver=Masternode.GENERIC)
#
#     def test_create_docker_machine(self):
#         creator = MasternodeCreator(self.masternode)
#         creator.create_server()
#         self.assertEqual(self.masternode.machine_status, Masternode.CREATING)
#
#         list_machines = creator.machine.ls()
#         # FIXME: there is always one active machine with empty name
#         self.assertEqual(len(list_machines), 2, "Machine should be available in: $ docker-machine ls")
#
#         # Ping machine via docker
#         creator.provision_server()
#
#         # Update machine info
#         creator.update_info_server()
#
#         url = reverse("pool:masternode_info", kwargs={'masternode_priv_key': self.masternode_priv_key})
#         response = self.client.get(url)
#
#         print("Info about new machine: " + str(response.data))
#         self.assertIsNotNone(response.data['machine_created_at'])
#         self.assertEqual(response.data['id'], self.masternode.id)
#         self.assertEqual(response.data['masternode_priv_key'], self.masternode.masternode_priv_key)
#         self.assertEqual(response.data['collateral_output_txid'], self.masternode.collateral_output_txid)
#         self.assertEqual(response.data['collateral_output_index'], self.masternode.collateral_output_index)
#         self.assertEqual(response.data['testnet'], self.masternode.testnet)
#         self.assertEqual(response.data['machine']['ip'], settings.GENERIC_IP_ADDRESS)

#FIXME add tests for vultr
#FIXME add test to check 5 minutes problem

class PoolCheckConfirmedAndSpentTransaction(APITestCase):
    def setUp(self):
        settings.DEBUG = True
        self.mn = Masternode.objects.create_masternode(masternode_priv_key="91yDd57Dtcba5x6Dgs32KNQAAyUwEBxUpM6gH2cgvdza4XYkQtq",
                                                              collateral_output_txid="9ee99e51bb1d3b49d804f2da424666921cfb47fc2d1131b9c55bf1f9a871178e",
                                                              collateral_output_index=0,
                                                              testnet=True,
                                                              machine_driver=Masternode.VULTR)

    def test_check_masternode_tx_is_confirmed(self):
        checker = RPCChecker(self.mn)
        is_confirmed = checker.is_tx_confirmed()
        self.assertEqual(is_confirmed, True)

    def test_check_masternode_tx_is_spent(self):
        checker = RPCChecker(self.mn)
        is_confirmed = checker.is_tx_spent()
        self.assertEqual(is_confirmed, True)

