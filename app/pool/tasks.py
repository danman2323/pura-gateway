from celery import task
from .models import Masternode, MasternodeReadyToDelete
from .provisioning.creator import MasternodeCreator, MasternodeCreateError
from .provisioning.rpc_checker import RPCChecker
from subprocess import call
from mngateway import settings

import logging

logger = logging.getLogger(__name__)

@task()
def sync_masternodes_deployments():

    # Get list of masternodes with status None
    masternodes = Masternode.objects.filter(machine_status__in=[Masternode.NONE])

    mn_count = len(masternodes)
    mn_created = 0
    mn_provisioned = 0

    logger.info('[sync_masternodes_deployments] Running command.')
    logger.info('[sync_masternodes_deployments] Found: ' + str(mn_count) + ' masternodes ready to deploy.')

    for masternode in masternodes:
        creator = MasternodeCreator(masternode)
        checker = RPCChecker(masternode)

        if not checker.is_tx_confirmed():
            logger.info('[sync_masternodes_deployments] Cannot create masternode: ' + masternode.get_machine_name() + ". TX is not confirmed")
            continue

        # TODO: implement it in future
        # if checker.is_tx_spent():
        #     logger.info('[sync_masternodes_deployments] Cannot create masternode: ' + masternode.get_machine_name() + ". TX out is spent")
        #     continue

        try:
            creator.create_server()
        except MasternodeCreateError as error:
            logger.error('[sync_masternodes_deployments] Cannot create masternode. ' + masternode.get_machine_name())
            continue
        mn_created += 1

        try:
            creator.provision_server()
        except MasternodeCreateError as error:
            logger.error('[sync_masternodes_deployments] Cannot provision masternode.' + masternode.get_machine_name())
            continue
        mn_provisioned += 1

    logger.info('[sync_masternodes_deployments] created: ' + str(mn_created) + '/' + str(mn_count) + ' | provisioned: ' + str(mn_provisioned) + '/' + str(mn_count))


    # Get list of masternodes with status STOP
    masternodes_ready_to_delete = MasternodeReadyToDelete.objects.all()
    mn_count = len(masternodes_ready_to_delete)
    mn_removed = 0

    logger.info('[sync_masternodes_deployments] Found: ' + str(mn_count) + ' masternodes ready to delete.')

    for mn in masternodes_ready_to_delete:
        masternode = mn.masternode
        creator = MasternodeCreator(masternode)
        try:
            creator.remove_server()
            mn.delete()
            masternode.delete()
        except MasternodeCreateError as error:
            logger.error('[sync_masternodes_deployments] Cannot remove masternode.')
            continue
        mn_removed += 1

    logger.info('[sync_masternodes_deployments] removed: ' + str(mn_removed) + '/' + str(mn_count))

    call(["s3cmd", "sync", "--delete-removed", "/root/.docker/machine", "s3://" + settings.AWS_BUCKET])

@task()
def sync_masternodes_updates():
    # Get list of masternodes with status UPDATING
    masternodes = Masternode.objects.filter(machine_status__in=[Masternode.UPDATING])
    mn_count = len(masternodes)
    mn_updated = 0

    logger.info('[sync_masternodes_deployments] Found: ' + str(mn_count) + ' masternodes ready to update.')

    for masternode in masternodes:
        creator = MasternodeCreator(masternode)
        try:
            creator.update_server()
        except RuntimeError as error:
            logger.error('[sync_masternodes_deployments] Cannot update masternode.')
            continue
        mn_updated += 1

    logger.info('[sync_masternodes_deployments] updated: ' + str(mn_updated) + '/' + str(mn_count))

@task()
def sync_masternodes_info():
    # Get list of masternodes with status Running
    masternodes = Masternode.objects.filter(machine_status__in=[Masternode.RUNNING])
    mn_count = len(masternodes)
    mn_gathered = 0
    mn_failed = 0

    for masternode in masternodes:
        creator = MasternodeCreator(masternode)
        try:
            creator.update_info_server()
        except RuntimeError as error:
            logger.error('[sync_masternodes_info] error: ' + str(error))
            mn_failed += 1
            continue
        mn_gathered += 1

    logger.info('[sync_masternodes_info] info gathered: ' + str(mn_gathered) + '/' + str(mn_count) + ' | failed: ' + str(
        mn_failed) + '/' + str(mn_count))
