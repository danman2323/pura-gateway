from django.db import models
from datetime import datetime
from mngateway import settings
from datetime import datetime
import json

class MasternodeManager(models.Manager):
    def create_masternode(self, masternode_priv_key, collateral_output_txid, collateral_output_index,testnet, machine_driver='vultr'):
        masternode = self.create(masternode_priv_key=masternode_priv_key,
                                 collateral_output_txid=collateral_output_txid,
                                 collateral_output_index=collateral_output_index,
                                 testnet=testnet,
                                 machine_driver=machine_driver,
                                 machine_status=Masternode.NONE)
        masternode.save()
        return masternode

class Masternode(models.Model):

    DEFAULT_MACHINE_NAME = 'push-to-deploy'

    NONE            = "NON"
    CREATING        = "CRE"
    PROVISIONING    = "PRO"
    RUNNING         = "RUN"
    STOPPED         = "STO"
    FAILED          = "FAI"
    DELETED         = "DEL"
    UPDATING         = "UPD"

    MN_STATE_CHOICES = (
        (NONE           , 'None'),
        (CREATING       , 'Creating'),
        (PROVISIONING   , 'Provisioning'),
        (RUNNING        , 'Running'),
        (STOPPED        , 'Stopped'),
        (FAILED         , 'Failed'),
        (UPDATING       , 'Updating')
    )

    # Use it to automatically create bare-metal server on vultr.io
    VULTR   = "vultr"

    # Generic driver is convenient to provision docker on some vps with ssh. Implemented only for tests
    GENERIC = "generic"

    MACHINE_DRIVER_CHOICES = (
        (VULTR  , 'Vultr'),
        (GENERIC, 'Generic')
    )

    # Date fields
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    # Date that represents when machine was started
    machine_created_at = models.DateTimeField(null=True, blank=False)

    # Represents status of hardware
    machine_status = models.CharField(max_length=3, choices=MN_STATE_CHOICES, default=NONE)
    machine_logs = models.TextField(null=True, blank=True)

    # Masternode machine info
    machine_info = models.TextField(null=True, blank=True, default='')
    machine_driver = models.CharField(max_length=15, choices=MACHINE_DRIVER_CHOICES, default=VULTR)

    # Represents unique mn params
    masternode_priv_key = models.CharField(max_length=256, null=False, blank=False, unique=True)
    collateral_output_txid = models.CharField(max_length=256, null=False, blank=False)
    collateral_output_index = models.IntegerField(null=False, blank=False)

    # Docker container info
    container_status = models.TextField(null=True, blank=True, default='Unknown')
    container_mn_debug = models.TextField(null=True, blank=True, default='')
    container_mn_debug_update_date = models.DateTimeField(auto_now_add=True)
    container_blockchain_synced = models.BooleanField(default=False)

    # Testenet or mainnet
    testnet = models.BooleanField(default=True)

    objects = MasternodeManager()

    def append_machine_log(self, log):
        if self.machine_logs is None:
            self.machine_logs = ""
        self.machine_logs += '\n [' + str(datetime.now()) + '] ' + log
        self.save()

    def get_machine_name(self):
        # if self.testnet:
        #     return settings.MACHINE_NAME_PREFIX + '-testnet-' + Masternode.DEFAULT_MACHINE_NAME +'-' + str(self.id)
        return settings.MACHINE_NAME_PREFIX + '-' + Masternode.DEFAULT_MACHINE_NAME + '-' + str(self.id)

    def set_machine_info(self, info_object):
        self.machine_info = json.dumps(info_object)
        self.save()

    def set_machine_status(self, status):
        if status is Masternode.PROVISIONING and self.machine_status is not Masternode.PROVISIONING:
            self.machine_created_at = datetime.now()

        self.machine_status = status
        self.save()

    def set_container_status(self, status):
        self.container_status = status
        self.save()

    def set_container_mn_debug(self, debug):
        if debug != self.container_mn_debug:
            self.container_mn_debug_update_date = datetime.now()
            self.container_mn_debug = debug
            self.save()

    def set_container_blockchain_synced(self, status):
        self.container_blockchain_synced = status
        self.save()

    def can_create_server(self):
        return self.machine_status == Masternode.NONE

    def can_provision_server(self):
        return self.machine_status == Masternode.CREATING

    def can_update_server(self):
        return self.machine_status == Masternode.UPDATING

    def can_inspect(self):
        return self.machine_status == Masternode.RUNNING

    def can_kill_server(self):

        if self.machine_driver in [Masternode.GENERIC]:
            return self.machine_status in [Masternode.NONE, Masternode.RUNNING, Masternode.FAILED]

        if (self.machine_status in [Masternode.RUNNING]) and (self.machine_created_at is not None):
            time_diff = datetime.now().replace(tzinfo=None).timestamp() - self.machine_created_at.timestamp()
            return time_diff > 300

        return self.machine_status in [Masternode.NONE, Masternode.FAILED]


class MasternodeReadyToDeleteManager(models.Manager):
    def create_with_mn(self, masternode):
        masternode_to_delete = self.create(masternode=masternode)
        masternode_to_delete.save()
        return masternode_to_delete

class MasternodeReadyToDelete(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    masternode = models.OneToOneField(Masternode, on_delete=models.CASCADE, null=False, blank=False)
    objects = MasternodeReadyToDeleteManager()