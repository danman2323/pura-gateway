from docker.errors import DockerException
from ..models import Masternode
from machine import Machine
from docker import DockerClient
from mngateway import settings
import json

import logging

logger = logging.getLogger(__name__)

class Error(Exception):
    pass

class MasternodeCreateError(Error):
    def __init__(self, expression='', message=''):
        self.expression = expression
        self.message = message

class MasternodeCreator:

    MACHINE_PROCESS_PATH = '/usr/local/bin/docker-machine'

    def __init__(self, masternode):
        self.masternode = masternode
        self.machine = Machine(path=MasternodeCreator.MACHINE_PROCESS_PATH)

    def create_server(self):
        if not self.masternode.can_create_server():
            self.__raise('Cannot create server. Server is already created.')

        machine_create_args = None
        if self.masternode.machine_driver == Masternode.VULTR:
            machine_create_args = [
                '--vultr-api-key=' + settings.VULTER_API_KEY,
                '--vultr-os-id=' + str(settings.VULTER_OS_ID),
                '--vultr-region-id=' + str(settings.VULTER_REGION_ID),
                '--vultr-plan-id=' + str(settings.VULTER_PLAN_ID),
            ]

        self.masternode.set_machine_status(Masternode.CREATING)
        machine_create_err_code = self.machine.create(self.masternode.get_machine_name(), self.masternode.machine_driver, True, machine_create_args)
        if machine_create_err_code > 0:
            self.masternode.set_machine_status(Masternode.FAILED)
            error_msg = 'Cannot create server. docker-machine create respond with code: ' + str(machine_create_err_code)
            self.__raise(error_msg)
        else:
            self.__log('[MasternodeCreator] New machine created: ' + self.masternode.get_machine_name() )

        # Just additional check here
        if not self.__machine_exists_and_running():
            self.masternode.set_machine_status(Masternode.FAILED)
            self.__raise('Docker-machine is not exists after being created')

        machine_info = self.machine.inspect(self.masternode.get_machine_name())
        self.__log('[MasternodeCreator] New machine info: ' + str(machine_info))

        return machine_info

    def provision_server(self):
        if not self.masternode.can_provision_server():
            self.__raise("Cannot provision server. Server wasn't created.")

        docker_config = self.machine.config(self.masternode.get_machine_name())
        docker_client = DockerClient(**docker_config)

        if not docker_client.ping():
            self.__raise("Cannot provision server. Server is unavailable.")

        self.masternode.set_machine_status(Masternode.PROVISIONING)

        try:
            raw_info = self.machine.inspect(self.masternode.get_machine_name())
            host = raw_info.get('Driver', {}).get('IPAddress', "")

            docker_client.images.pull(settings.PURA_DOCKER_CONTAINER)
            docker_client.volumes.create('purad-data')
            container = docker_client.containers.run(
                settings.PURA_DOCKER_CONTAINER,
                name='purad',
                detach=True,
                ports={
                    '44444/tcp': '44444',
                    '44443/tcp': '44443',
                    '55555/tcp': '55555',
                    '55554/tcp': '55554'
                },
                volumes={
                    'purad-data': {
                        'bind': '/pura',
                        'mode': 'rw'
                    },
                },
                environment=[
                    'RPCUSER='+self.masternode.get_machine_name(),
                    'RPCPASSWORD='+'password', #FIXME: change to more secure
                    'TESTNET='+ ('1' if self.masternode.testnet else '0'),
                    'MASTERNODE_ALIAS='+self.masternode.get_machine_name(),
                    'MASTERNODE_IP='+host,
                    'MASTERNODE_PORT='+str(44443 if self.masternode.testnet else 44444),
                    'MASTERNODE_PRIV_KEY='+self.masternode.masternode_priv_key,
                    'MASTERNODE_COLLATERAL_OUTPUT_TXID='+self.masternode.collateral_output_txid,
                    'MASTERNODE_COLLATERAL_OUTPUT_INDEX='+str(self.masternode.collateral_output_index),
                    'DISABLEWALLET=0'
                ],
                log_config={
                    'type': 'json-file',
                    'config':  {
                        "max-size": "10m",
                        "max-file": "3"
                    }
                }
            )
            self.__log('[MasterccnodeCreator] Running pura-docker container [' + container.id + '] on host: [' + self.masternode.get_machine_name() + ']')

        except DockerException as error:
            self.__log('[MasternodeCreator] Unable to start pura-docker image in the container. ' + str(error))
            self.masternode.set_machine_status(Masternode.FAILED)
            self.__raise('Cannot start pura-docker image. ' + str(error)) #FIXME: handle docker exceptions better

        self.masternode.set_machine_status(Masternode.RUNNING)

    def remove_server(self):
        if self.masternode.can_kill_server() or self.__machine_exists_and_running():

            try:
                docker_config = self.machine.config(self.masternode.get_machine_name())
            except RuntimeError as error:
                result = self.machine.rm(self.masternode.get_machine_name(), False)
                self.__log(
                    "[MasternodeCreator] No keys found for masternode: " + self.masternode.get_machine_name() + ". Forced deletion. Result: " + str(result))
                return

            docker_client = DockerClient(**docker_config)

            try:
                for container in docker_client.containers.list():
                    self.__log("[MasternodeCreator] Stopping container: " + container.id + " on host: " + self.masternode.get_machine_name())
                    container.stop()
                    self.__log("[MasternodeCreator] Deleting stopped containers on host: " + self.masternode.get_machine_name())
                docker_client.containers.prune()

                for volume in docker_client.volumes.list():
                    self.__log("[MasternodeCreator] Deleting volume: " + volume.name + " from host: " + self.masternode.get_machine_name())
                    volume.remove()

            except DockerException as error:
                self.masternode.set_machine_status(Masternode.FAILED)
                self.__log('[MasternodeCreator] Unable to teardown containers on host: ' + self.masternode.get_machine_name() + '' + str(error))
                self.__raise('Unable to teardown containers')

            result = self.machine.rm(self.masternode.get_machine_name(), False)
            if result:
                self.masternode.set_machine_status(Masternode.DELETED)
                self.__log("[MasternodeCreator] docker-machine: " + self.masternode.get_machine_name() + ". Machine was successfully removed")
            else:
                self.__log("[MasternodeCreator] Unable to remove docker-machine: " + self.masternode.get_machine_name() + ". 'docker-machine rm' returns false.")
                self.__raise("Unable to remove docker-machine. Command 'docker-machine rm' returns false.")

    def update_server(self):

        self.__log("[MasternodeCreator] Updating masternode: " + self.masternode.get_machine_name() + " status: " + self.masternode.machine_status)

        if self.masternode.can_update_server() or self.__machine_exists_and_running():

            self.masternode.set_machine_status(Masternode.CREATING)

            self.__log("[MasternodeCreator] Masternode: " + self.masternode.get_machine_name() + " running")

            docker_config = self.machine.config(self.masternode.get_machine_name())
            docker_client = DockerClient(**docker_config)

            try:
                for container in docker_client.containers.list():
                    self.__log("[MasternodeCreator] Stopping container: " + container.id + " on host: " + self.masternode.get_machine_name())
                    container.stop()
                    self.__log("[MasternodeCreator] Deleting stopped containers on host: " + self.masternode.get_machine_name())
                docker_client.containers.prune()

                for volume in docker_client.volumes.list():
                    self.__log("[MasternodeCreator] Deleting volume: " + volume.name + " from host: " + self.masternode.get_machine_name())
                    volume.remove()

            except DockerException as error:
                self.masternode.set_machine_status(Masternode.FAILED)
                self.__log('[MasternodeCreator] Unable to teardown containers on host: ' + self.masternode.get_machine_name() + '' + str(error))
                self.__raise('Unable to teardown containers')
                return

            self.masternode.set_machine_status(Masternode.CREATING)
            self.provision_server()
        else:
            self.__log("[MasternodeCreator] Masternode: " + self.masternode.get_machine_name() + " is running: " + str(self.__machine_exists_and_running()))

    def update_info_server(self):
        self.__log('[MasternodeCreator] updating info for masternode: ' + str(self.masternode.id))
        machine_info = {}
        if self.masternode.can_inspect():
            raw_info = self.machine.inspect(self.masternode.get_machine_name())
            machine_info = {
                'ip': raw_info.get('Driver', None).get('IPAddress', None),
                'name': raw_info.get('Driver', None).get('MachineName', None),
            }
        self.masternode.set_machine_info(machine_info)
        self.update_info_wallet()

    def update_info_wallet(self):
        self.__log('[MasternodeCreator] updating info of container in masternode: ' + str(self.masternode.id))
        docker_config = self.machine.config(self.masternode.get_machine_name())
        docker_client = DockerClient(**docker_config)

        if not docker_client.ping():
            self.__raise("Cannot provision server. Server is unavailable.")

        containers = docker_client.containers.list(all=True, filters={
            'name': 'purad'
        })

        if len(containers) > 0:
            container = containers[0]
            self.masternode.set_container_status(container.status)

            exit_code, output = container.exec_run("pura-cli masternode debug")
            self.masternode.set_container_mn_debug(output.decode("utf-8"))

            exit_code, output = container.exec_run("pura-cli mnsync status")
            dict = json.loads(output.decode("utf-8"))
            self.masternode.set_container_blockchain_synced(dict["IsSynced"])


    def __machine_exists_and_running(self):
        exists = self.machine.exists(self.masternode.get_machine_name())
        return exists

    def __raise(self, error_msg):
        print("[MasternodeCreator][MasternodeCreateError]: " + error_msg)
        logger.exception("[MasternodeCreator][MasternodeCreateError]: " + error_msg)
        self.masternode.append_machine_log("[MasternodeCreator][MasternodeCreateError]: " + error_msg)
        raise MasternodeCreateError(message=error_msg)

    def __log(self, log_msg):
        # self.masternode.append_machine_log("[MasternodeCreator]: " + log_msg)
        logger.info("[MasternodeCreator]: " + log_msg)
        print("[MasternodeCreator]: " + log_msg)