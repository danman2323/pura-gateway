from ..models import Masternode
from mngateway import settings
import json
import logging
import requests
import uuid

logger = logging.getLogger(__name__)

class Error(Exception):
    pass

class RPCCheckerError(Error):
    def __init__(self, expression='', message=''):
        self.expression = expression
        self.message = message

class RPCChecker:

    def __init__(self, masternode):
        self.masternode = masternode

    def is_tx_confirmed(self):
        result = self.__rpc_request("getrawtransaction", [
            self.masternode.collateral_output_txid,
            1
        ])
        return result.get("confirmations", 0) > 2

    def is_tx_spent(self):
        result = self.__rpc_request("gettxout", [
            self.masternode.collateral_output_txid,
            self.masternode.collateral_output_index
        ])
        # logger.info("[RPCChecker][is_tx_spent]: " + str(result is not None))
        return result is not None

    def __rpc_request(self, method, params):
        url = self.__rpc_url()
        params = self.__rpc_params(method, params)
        headers = self.__rpc_headers()

        r = requests.post(url=url,
                          data=params,
                          headers=headers)

        # logger.info(
        #     "[RPCChecker][rpc_request] url:" + url + " params: " + params + " headers: " + str(headers) + " response: " + r.text)

        return r.json().get("result", None)

    def __rpc_headers(self):
        auth = settings.PURA_TESTNET_AUTH if self.masternode.testnet else settings.PURA_MAINNET_AUTH
        return { 'Authorization': auth }

    def __rpc_params(self, method, params):
        return json.dumps({
            "jsonrpc": "1.0",
            "method": method,
            "params": params,
            "id": str(uuid.uuid4())
        })

    def __rpc_url(self):
        if self.masternode.testnet:
            return "http://" + settings.PURA_TESTNET_HOST + ":" + settings.PURA_TESTNET_PORT

        return "http://" + settings.PURA_MAINNET_HOST + ":" + settings.PURA_MAINNET_PORT