from rest_framework import generics, permissions, status, views, mixins
from rest_framework.response import Response
from .serializers import MasternodeSerializer
from .models import Masternode, MasternodeReadyToDelete

class MasternodeViewListAPIView(generics.ListAPIView):

    serializer_class = MasternodeSerializer
    permission_classes = (permissions.AllowAny,)

    def get_queryset(self):
        keys = self.request.data.get('masternode_priv_keys', None)
        if keys != None:
            return Masternode.objects.filter(masternode_priv_key__in=self.request.data['masternode_priv_keys'])

        return None

    def post(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


class MasternodeViewAPIView(generics.RetrieveAPIView, mixins.DestroyModelMixin):
    lookup_field = 'masternode_priv_key'
    serializer_class = MasternodeSerializer
    permission_classes = (permissions.AllowAny,)

    def get_queryset(self):
        masternode_priv_key = self.kwargs.get('masternode_priv_key', None)
        return Masternode.objects.filter(masternode_priv_key=masternode_priv_key)

    def delete(self, request, *args, **kwargs):
        masternodes = self.get_queryset()

        if len(masternodes) == 0:
            return Response(status=status.HTTP_204_NO_CONTENT)

        for masternode in masternodes:
            ready_to_delete = MasternodeReadyToDelete.objects.create_with_mn(masternode)

        return self.get(request, args, kwargs)


class MasternodeCreateAPIView(generics.GenericAPIView, mixins.CreateModelMixin):

    lookup_field = 'masternode_priv_key'
    serializer_class = MasternodeSerializer
    permission_classes = (permissions.AllowAny,)

    def get_queryset(self):
        masternode_priv_key = self.kwargs.get('masternode_priv_key', None)
        return Masternode.objects.filter(masternode_priv_key=masternode_priv_key)

    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)