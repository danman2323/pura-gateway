from django.contrib import admin
from django.core.handlers.wsgi import WSGIRequest
from .models import Masternode, MasternodeReadyToDelete

class MasternodeModelAdmin(admin.ModelAdmin):

    def has_delete_permission(self, request, obj=None):
        return False

    def get_readonly_fields(self, request, obj=None):
        readonly_fields = super(MasternodeModelAdmin, self).get_readonly_fields(request, obj)

        if request.path.find('add') >= 0:
            return ['machine_status', 'machine_logs', 'machine_info', 'machine_created_at', 'machine_driver', 'container_status', 'container_mn_debug', "container_mn_debug_update_date", "container_blockchain_synced"]

        return ['machine_logs', 'machine_info', 'masternode_priv_key', 'collateral_output_txid', 'collateral_output_index', 'testnet', 'machine_created_at', 'machine_driver', 'container_status', 'container_mn_debug', 'created_at', 'updated_at', "container_mn_debug_update_date", "container_blockchain_synced" ]

    def get_actions(self, request):
        actions = super(MasternodeModelAdmin, self).get_actions(request)
        del actions['delete_selected']
        return actions

    list_filter = ['machine_status', 'container_status', 'container_mn_debug', 'testnet', 'created_at', 'updated_at', 'container_blockchain_synced']
    list_editable = ['machine_status']
    list_display = ['id', 'created_at', 'updated_at', 'machine_info', 'machine_status', 'container_status', 'container_mn_debug', 'testnet', 'container_blockchain_synced']

# Register your models here.
admin.site.register(Masternode, MasternodeModelAdmin)
admin.site.register(MasternodeReadyToDelete)