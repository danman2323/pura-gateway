from django.urls import path
from .views import MasternodeCreateAPIView, MasternodeViewAPIView, MasternodeViewListAPIView

app_name = 'pool'

urlpatterns = [
    path('masternode/<masternode_priv_key>', MasternodeViewAPIView.as_view(), name='masternode_info'),
    path('masternode/', MasternodeCreateAPIView.as_view(), name='masternode_create'),
    path('masternodes/', MasternodeViewListAPIView.as_view(), name='masternode_list_info')
]