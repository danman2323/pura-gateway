#!/bin/sh
echo "app: waiting postgress"
while ! nc -z $DATABASE_HOST $DATABASE_PORT; do
  sleep 0.5
done

echo "app: waiting app_setup"
sleep 10
echo "app: ready"

python manage.py runserver 0.0.0.0:8000