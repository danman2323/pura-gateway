#!/usr/bin/env bash

mkdir -p /root/.docker
s3cmd get -r s3://$AWS_BUCKET/machine /root/.docker/
celery -A mngateway worker -l info -c 1