#!/bin/bash

# Prepare log files and start outputting logs to stdout
mkdir -p ./log
touch ./log/gunicorn.log
touch ./log/gunicorn-access.log
tail -n 0 -f ./log/gunicorn*.log &

export DJANGO_SETTINGS_MODULE=mngateway.settings

exec gunicorn mngateway.wsgi:application \
    --name mngateway_django \
    --bind 0.0.0.0:8000 \
    --workers 5 \
    --log-level=info \
    --log-file=./log/gunicorn.log \
    --access-logfile=./log/gunicorn-access.log
#exec gunicorn iconx.wsgi:application \
#    --name iconx_django \
#    --bind 0.0.0.0:8000 \
#    --workers 5 \
#    --log-config=./gunicorn.conf
"$@"