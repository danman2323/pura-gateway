#!/bin/sh
# Run from within ./kibana directory

USERNAME=seductive
IMAGE=ixkibana

docker build -t ${USERNAME}/${IMAGE}:latest .
docker push ${USERNAME}/${IMAGE}:latest
