#!/bin/sh

MN_ALIAS='amazon-staging-gateway-ubuntu-40'
MN_IP='149.28.231.98'
MN_PRIV_KEY='921oC3rLALHJqdio7UN5ApMByhzy55PsN4L5QUR6oRAa7KEo89G'
MN_OUTPUT_TX='263364b78910bce7311898c7b509344b4028cbbefb2ee50d51f0039ab9973813'
MN_OUTPUT_ID='1'
TESTNET=1

docker stop purad
docker rm purad
docker volume rm purad-data
docker pull seductivecto/pura-docker:latest
docker volume create purad-data

docker run -d \
--name purad \
--volume purad-data:/root/.pura \
--env RPCUSER=$MN_ALIAS \
--env RPCPASSWORD=password \
--env TESTNET=$TESTNET \
--env MASTERNODE_ALIAS=$MN_ALIAS \
--env MASTERNODE_IP=$MN_IP \
--env MASTERNODE_PORT=44443 \
--env MASTERNODE_PRIV_KEY=$MN_PRIV_KEY \
--env MASTERNODE_COLLATERAL_OUTPUT_TXID=$MN_OUTPUT_TX \
--env MASTERNODE_COLLATERAL_OUTPUT_INDEX=$MN_OUTPUT_ID \
-p 44444:44444 \
-p 44443:44443 \
-p 55555:55555 \
-p 55554:55554 \
seductivecto/pura-docker:latest