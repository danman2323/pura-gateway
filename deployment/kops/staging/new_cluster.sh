#!/bin/sh

# Kubernetes / Kops configuration for AWS staging cluster
export AWS_ACCESS_KEY_ID=AKIAJITIIPWAW2YM2ERQ
export AWS_SECRET_ACCESS_KEY=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
export AWS_PROFILE=pura
export AWS_ZONE=ap-southeast-1
export AWS_SUBZONE=ap-southeast-1a
export NAME=pu-ga.com
export S3_BUCKET=pu-ga-com-state-store
export KOPS_STATE_STORE=s3://${S3_BUCKET}
export MASTER_SIZE=t2.micro
export NODE_SIZE=t2.medium
export NODE_COUNT=2

# Create AWS storage for cluster settings
aws s3api create-bucket --bucket ${S3_BUCKET} --region ${AWS_ZONE}

# Removing cluster
# kops delete cluster --name=${NAME} --yes

kops create cluster --name=${NAME} --state=${KOPS_STATE_STORE} --zones=${AWS_SUBZONE} --node-count=${NODE_COUNT} --node-size ${NODE_SIZE} --master-size ${MASTER_SIZE}

# kops get cluster
# kops edit cluster ${NAME}
# kops edit ig --name=${NAME} nodes
# kops edit ig --name=${NAME} master-eu-west-1c

kops edit ig --name=${NAME} --state=${KOPS_STATE_STORE}
kops update cluster ${NAME} --node-count=${NODE_COUNT} --node-size ${NODE_SIZE} --master-size ${MASTER_SIZE} --yes

# Wait about 10-15 minutes till Kops will set up the cluster
# Check cluster readiness
kubectl get nodes --show-labels
kops validate cluster


# Installing monitoring & dashboard
kubectl apply --filename https://raw.githubusercontent.com/kubernetes/heapster/master/deploy/kube-config/rbac/heapster-rbac.yaml
kubectl apply --filename https://raw.githubusercontent.com/kubernetes/heapster/master/deploy/kube-config/standalone/heapster-controller.yaml
kubectl apply -f https://raw.githubusercontent.com/kubernetes/dashboard/master/src/deploy/recommended/kubernetes-dashboard.yaml

# Create assets storage on S3
# aws s3api create-bucket --bucket iconx-assets-staging --region eu-west-1c

# Private repo access
# https://kubernetes.io/docs/tasks/configure-pod-container/pull-image-private-registry/

# Amazon Route53:
# Find loadbalancer and change primary domain to this load balancer