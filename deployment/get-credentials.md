Kubernetes dashboard
===

``` bash
kubectl apply --filename https://raw.githubusercontent.com/kubernetes/heapster/master/deploy/kube-config/rbac/heapster-rbac.yaml
kubectl apply --filename https://raw.githubusercontent.com/kubernetes/heapster/master/deploy/kube-config/standalone/heapster-controller.yaml
kubectl apply -f https://raw.githubusercontent.com/kubernetes/dashboard/master/src/deploy/recommended/kubernetes-dashboard.yaml
```

To open kubernetes dashboard use link
===
Write in terminal

``` bash
kubectl proxy
```

[Then open link in your browser](http://localhost:8001/api/v1/namespaces/kube-system/services/https:kubernetes-dashboard:/proxy/#!/overview?namespace=default)

Deployment process
===

1. redis
2. security
3. app setup 
4. app
5. worker
6. worker beat 

For deployments
===

``` bash
kubectl apply -f deployment/kubernetes/staging/deployments/postgres.yml
```

For jobs and security
===

``` bash
kubectl create -f deployment/kubernetes/staging/jobs/appsetup.yml
```

To create regcred
===

[Pull image from private registry](https://kubernetes.io/docs/tasks/configure-pod-container/pull-image-private-registry/)

``` bash 
kubectl create secret docker-registry regcred \
--docker-server=https://index.docker.io/v1/ \
--docker-username=seductivecto \
--docker-password=inside90 \
--docker-email=cto@seductive.com.ua
```

``` bash
kubectl get secret regcred --output="jsonpath={.data.\.dockerconfigjson}" | base64 -D 
```

